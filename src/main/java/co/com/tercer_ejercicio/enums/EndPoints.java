package co.com.tercer_ejercicio.enums;

public enum EndPoints {

    RUTA_USER_AUTH("/auth");

    private final String value;
    EndPoints(String value) {this.value = value;}
    public String route() {return value;}

}
