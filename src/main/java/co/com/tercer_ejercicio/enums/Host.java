package co.com.tercer_ejercicio.enums;

public enum Host {

    HOST_USER_AUTH("https://restful-booker.herokuapp.com");

    private final String value;
    Host(String value) {this.value = value;}
    public String url() {return value;}

}
