package co.com.tercer_ejercicio.enums;

public enum KeysUserAuth {

    KEY1("Status"),
    KEY2("Token User"),
    KEY3("Reason");



    private final String value;
    KeysUserAuth(String value) {this.value = value;}
    public String value() {
        return value;
    }

}
