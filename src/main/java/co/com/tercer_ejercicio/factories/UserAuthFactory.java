package co.com.tercer_ejercicio.factories;

import co.com.tercer_ejercicio.exceptions.NotFoundException;
import co.com.tercer_ejercicio.models.UserAuthModel;
import lombok.SneakyThrows;

import java.io.FileNotFoundException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

import static io.restassured.path.json.JsonPath.from;

public class UserAuthFactory {

    private UserAuthFactory() {
    }

    public static final String DATA_PATH = "json/user_auth.json";
    private static final String VALUE = "admin";

    public static UserAuthModel withDataDefault() {
        return getData().stream().filter(data -> data.getUsername().equals(VALUE)).findFirst().
                orElseThrow(() ->
                        new NotFoundException(String.format("client with value %s not found", VALUE)));
    }

    public static List<UserAuthModel> getData() {
        return Arrays.asList(from(getDataFile()).getObject("data", UserAuthModel[].class));
    }

    @SneakyThrows
    public static URL getDataFile() {
        String path = DATA_PATH;
        URL filePath = UserAuthFactory.class.getClassLoader().getResource(path);
        if (filePath == null) {
            throw new FileNotFoundException("File not found for client: " + path);
        }
        return filePath;
    }

}
