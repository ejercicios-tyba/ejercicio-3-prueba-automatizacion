package co.com.tercer_ejercicio.models;

import lombok.Data;

@Data
public class UserAuthModel {

    private String username;
    private String password;

}
