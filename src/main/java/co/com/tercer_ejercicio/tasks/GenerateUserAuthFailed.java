package co.com.tercer_ejercicio.tasks;

import co.com.tercer_ejercicio.enums.KeysUserAuth;
import co.com.tercer_ejercicio.factories.UserAuthFactory;
import co.com.tercer_ejercicio.models.UserAuthModel;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.http.ContentType;
import lombok.SneakyThrows;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;
import net.serenitybdd.screenplay.rest.interactions.Post;
import org.apache.http.HttpHeaders;

import java.util.logging.Level;
import java.util.logging.Logger;

import static co.com.tercer_ejercicio.enums.EndPoints.RUTA_USER_AUTH;
import static co.com.tercer_ejercicio.enums.Host.HOST_USER_AUTH;
import static net.serenitybdd.screenplay.Tasks.instrumented;

public class GenerateUserAuthFailed implements Task {

    private final String username;
    private final String password;
    private final UserAuthModel userAuthModel;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public GenerateUserAuthFailed(String username, String password){
        this.username = username;
        this.password = password;
        this.userAuthModel = UserAuthFactory.withDataDefault();
    }

    public static GenerateUserAuthFailed authFailed(String username, String password){
        return instrumented(GenerateUserAuthFailed.class, username, password);
    }

    @SneakyThrows
    @Override
    public <T extends Actor> void performAs(T actor) {

        userAuthModel.setUsername(username);
        userAuthModel.setPassword(password);

        actor.whoCan(CallAnApi.at(HOST_USER_AUTH.url()));
        String json = objectMapper.writeValueAsString(userAuthModel);
        actor.attemptsTo(
                Post.to(RUTA_USER_AUTH.route())
                        .with(request -> request.
                                header(HttpHeaders.CONTENT_TYPE, ContentType.JSON).
                                body(json)
                        )
        );

        Logger.getAnonymousLogger().log(Level.INFO, "Estado: {0}", SerenityRest.lastResponse().statusCode());
        Logger.getAnonymousLogger().log(Level.INFO, "Reason: {0}", SerenityRest.lastResponse().jsonPath().getObject("reason", String.class));

        actor.remember(KeysUserAuth.KEY1.value(), SerenityRest.lastResponse().statusCode());
        actor.remember(KeysUserAuth.KEY3.value(), SerenityRest.lastResponse().jsonPath().getObject("reason", String.class));

    }


}
