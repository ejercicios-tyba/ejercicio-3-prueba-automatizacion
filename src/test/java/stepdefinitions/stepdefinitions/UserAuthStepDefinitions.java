package stepdefinitions.stepdefinitions;

import co.com.tercer_ejercicio.enums.KeysUserAuth;
import co.com.tercer_ejercicio.tasks.GenerateUserAuthFailed;
import co.com.tercer_ejercicio.tasks.GenerateUserAuthSuccessful;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import net.serenitybdd.screenplay.ensure.Ensure;

import static net.serenitybdd.screenplay.actors.OnStage.theActorCalled;
import static net.serenitybdd.screenplay.actors.OnStage.theActorInTheSpotlight;

public class UserAuthStepDefinitions {

    @Given("el {word} ingresa los datos {string} {string}")
    public void elUsuarioIngresaLosDatos(String name, String username, String password) {
        theActorCalled(name).wasAbleTo(GenerateUserAuthSuccessful.withdata(username,password));
    }

    @Then("el usuario puede autenticarse exitosamente")
    public void elUsuarioPuedeAutenticarseExitosamente() {
        theActorInTheSpotlight().wasAbleTo(
                Ensure.that("200").isEqualTo(theActorInTheSpotlight().recall(KeysUserAuth.KEY1.value()).toString()),
                Ensure.that("null").isNotEqualTo(theActorInTheSpotlight().recall(KeysUserAuth.KEY2.value()))
        );
    }

    @Given("el {word} ingresa con datos erroneos {string} {string}")
    public void elUsuarioIngresaConDatosErroneos(String name, String username, String password) {
        theActorCalled(name).wasAbleTo(GenerateUserAuthFailed.authFailed(username, password));
    }

    @Then("el usuario evidencia la autenticacion fallida")
    public void elUsuarioEvidenciaLaAutenticacionFallida() {
        theActorInTheSpotlight().wasAbleTo(
                Ensure.that("200").isEqualTo(theActorInTheSpotlight().recall(KeysUserAuth.KEY1.value()).toString()),
                Ensure.that("Bad credentials").isEqualTo(theActorInTheSpotlight().recall(KeysUserAuth.KEY3.value()))
        );
    }

}
