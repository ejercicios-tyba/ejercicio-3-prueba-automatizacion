#language: en

@UserAuth
Feature: El usuario realiza la autenticacion


  Scenario Outline: Autenticacion de usuario con resultado exitoso
    Given el usuario ingresa los datos "<username>" "<password>"
    Then el usuario puede autenticarse exitosamente

    Examples:
      | username | password    |
      | admin    | password123 |


  Scenario Outline: Autenticacion de usuario con resultado fallido
    Given el usuario ingresa con datos erroneos "<username>" "<password>"
    Then el usuario evidencia la autenticacion fallida

    Examples:
      | username | password     |
      | admin    | password1234 |
      | prueba   | password123  |
      | abcdef   | prueba123    |
